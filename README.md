# Smart Rain Gauge
## A project by [ICFOSS](https://icfoss.in/)

## Project Summary
Rainfall also known as precipitation is an important part of environment stability. Inorder to have a sustainable echo system, it is important for living beings to have access to clean drinking water. It is also important for early flood warning as well. Many efforts to attain this objective depends on accurate precipitation monitoring.

Precipitation monitoring devices are broadly classified into manuel, mechanical, and optical. It is desirable to have accurate sensors which are not manuel/mechanical/optical. This project is an attempt to predict precipitation using machine learning techniques with sound loudness as input feature.

## Features
 - Rainfall estimation using audio data
 - Prediction Accuracy of 78% compared to Davis mechanical rain gauge
 - Built and deployed deep learning model

## Prerequisites
 - [Raspberry Pi 4](https://en.wikipedia.org/wiki/Raspberry_Pi)
 - Ubuntu Server OS
 - [USB Mic, Jieli Technology UACDemoV1.0](https://www.amazon.in/USB-Microphone/s?k=USB+Microphone)
 - [Davis AeroCone 6466M Rain Gauge](https://www.amazon.de/-/en/Davis-AeroCone-6466M-Gauge-Sensor/dp/B08629NFVG)
 - ALSA  Utils
 - PulseAudio

## Getting Started
### DAQ Setup
The Raspberry Pi 4 connected to USB microphone is kept in the enclosure. Ubuntu Server OS is installed to Raspberry Pi 4. After booting up the following script is run to install the dependencies.

```console
cd Dependencies
chmod 777 setup.sh
./setup.sh
```

### Data
#### Data from USB mic and Raspberry
The data from USB mic contains audio files saved in wav format with a fixed duration. The parameters of desired audio files (e.g. sampling rate, sample duration, bit size, total recording time, etc.) can be set in the `config.yaml` file. The recorded audio files are further analysed for deep learning modeling.

The recorded wav files are saved with a timestamp (`yyyy_mm_dd_hh_mm_ss_millisec.wav`) file name as shown below.

`2023_11_06_16_13_11_011224.wav`

#### Datasets made available on Kaggle
1. [Rain_Data_Master_2023](https://www.kaggle.com/datasets/sajilck/rain-data-master-2023) contains all audio recordings and rainfall data from mechanical rainguage in overlapping time durations collected so far. Please note that files are of different duration (10 sec and 3 sec) and sampling rates (48K & 8K).

2. [Rain_Data_Master_8K](https://www.kaggle.com/datasets/sajilck/rain-data-master-8k) Contains downsamples version of [Rain_Data_Master_2023](https://www.kaggle.com/datasets/sajilck/rain-data-master-2023) so that all files are of same sample rate (i.e. 8K).

### Scripts
1. `daq_pi.py` contains Python script for automated audio recording in Raspberry which is added to the `~/.bashrc` profile so that the script is run everytime the device boots up and user logs in. The bashrc file is appended with the following command.
```console
python3 daq_pi.py
```
2. `mech_vs_non_mech_dataset_creation.ipynb` contains the Kaggle script to combine wave files recorded along with mechanical rain gauge data to create training data for deep learning modeling.

3. `seq_mech_vs_non_mech.ipynb` contains the LSTM modeling code which uses acoustic and mechanical data for rainfall estimation

## Results
#### LSTM Sequential Model Performance
| **EPOCHS** | **MODEL** | **MAPE** |
|------------|-----------|----------|
| 25         | LSTM      | 22%      |


## Contribution

### Contributors List
1. Gopika T G
2. [Sajil C K](https://github.com/cksajil/)
3. Manu Mohan M S
4. Aiswarya Babu 
5. Harikrishnan K P

### Contributing
Instructions coming up soon.

## Licensing
 - MIT License

## Acknowledgements
We thank the Government of Kerala for the financial support and the whole ICFOSS team for the help and support. We specially acknowledge and thank all the early members of the Automatic Weather Station project for laying the foundations of our work.

## Changelog